Prism.highlightAll();

var link = document.createElement( 'link' );
link.rel = 'stylesheet';
link.type = 'text/css';
link.href = window.location.search.match( /print-pdf/gi ) ? './style/print/pdf.css' : './style/print/paper.css';
document.getElementsByTagName( 'head' )[0].appendChild( link );

// More info about config & dependencies:
// - https://reveal-js-multiplex-ccjbegmaii.now.sh/token
Reveal.initialize({
  multiplex: {
		secret: 'xxxxxxxxxxxxxxxxxxx', // Obtained from the socket.io server. Gives this (the master) control of the presentation
		id: 'xxxxxxxxxxxxxxxxxxx', // Obtained from socket.io server
		url: 'https://reveal-js-multiplex-ccjbegmaii.now.sh' // Location of socket.io server
	},
  width: 1200,
	height: 900,
	margin: 0.1,
	minScale: 0.2,
	maxScale: 1.5,
  transition: 'slide', // none/fade/slide/convex/concave/zoom
	transitionSpeed: 'default', // default/fast/slow
  backgroundTransition: 'fade', // none/fade/slide/convex/concave/zoom
  slideNumber: 'c/t',
  controls: false,
  progress: true,
  history: true,
  dependencies: [
    { src: './script/plugin/markdown/marked.js' },
    { src: './script/plugin/markdown/markdown.js' },
    { src: './script/plugin/gamepad/gamepad.js', async: true },
    { src: './script/plugin/zoom-js/zoom.js', async: true },
    { src: './script/plugin/notes/notes.js', async: true },
    // Master
    { src: 'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js', async: true },
	{ src: './script/plugin/multiplex/master.js', async: true },
    { src: './script/plugin/notes-server/client.js', async: true }
  ]
});


Reveal.configure({
  keyboard: {
    40: 'next',
    38: 'prev',
    9: 'togglePause'
  }
});