var gulp = require('gulp'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    pug = require('gulp-pug'),
    pugLinter = require('gulp-pug-linter'),
    htmlmin = require('gulp-htmlmin'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    surge = require('gulp-surge'),
    gulpSequence = require('gulp-sequence'),
    zip = require('gulp-zip'),
    rename = require("gulp-rename"),
    browserSync = require('browser-sync').create();

var surgeUrl = 'model-pug-reveal.surge.sh';

var cssPlugins = [ cssnano() ];
var notes = ['node_modules/reveal.js/plugin/notes/notes.html']
var revealJs = ['node_modules/reveal.js/plugin/**/*.js'];
var styleReveal = ['node_modules/reveal.js/css/print/*.css'];

var pluginsReveal = [
  'node_modules/reveal-code-focus/reveal-code-focus.js',
  'node_modules/reveal.js-gamepad-plugin/gamepad.js'
];

var mainjs = [
  // Reveal
  'node_modules/reveal.js/lib/js/head.min.js',
  'node_modules/reveal.js/js/reveal.js',
  // Prism
  'node_modules/prismjs/components/prism-core.js',
  'node_modules/prismjs/components/prism-markup.js',
  'node_modules/prismjs/components/prism-css.js',
  'node_modules/prismjs/components/prism-clike.js',
  'node_modules/prismjs/components/prism-javascript.js',
  'node_modules/prismjs/components/prism-asciidoc.js',
  'node_modules/prismjs/components/prism-bash.js',
  'node_modules/prismjs/components/prism-sass.js',
  'node_modules/prismjs/components/prism-scss.js',
  'node_modules/prismjs/components/prism-rust.js',
  'node_modules/prismjs/components/prism-pug.js',
  'node_modules/prismjs/components/prism-go.js',
  'node_modules/prismjs/plugins/file-highlight/prism-file-highlight.js',
  // Main
  'src/script/main.js'
]

var mainMasterjs = [
  // Reveal
  'node_modules/reveal.js/lib/js/head.min.js',
  'node_modules/reveal.js/js/reveal.js',
  // Prism
  'node_modules/prismjs/components/prism-core.js',
  'node_modules/prismjs/components/prism-markup.js',
  'node_modules/prismjs/components/prism-css.js',
  'node_modules/prismjs/components/prism-clike.js',
  'node_modules/prismjs/components/prism-javascript.js',
  'node_modules/prismjs/components/prism-asciidoc.js',
  'node_modules/prismjs/components/prism-bash.js',
  'node_modules/prismjs/components/prism-sass.js',
  'node_modules/prismjs/components/prism-scss.js',
  'node_modules/prismjs/components/prism-rust.js',
  'node_modules/prismjs/components/prism-pug.js',
  'node_modules/prismjs/components/prism-go.js',
  'node_modules/prismjs/plugins/file-highlight/prism-file-highlight.js',
  // Main
  'src/script/main-master.js'
]

var minOptions = {collapseWhitespace: true, minifyCSS: true, minifyJS: false};

var destination = 'public';
var destinationScript = destination + '/script';
var destinationStyle = destination + '/style';
var pluginsDestination = destinationScript + '/plugin';

// Reveal
gulp.task('compress', ['copyStyle', 'copyNotesHtml', 'copyPlugins', 'mainjs', 'gamepad'], function (cb) {
  return gulp.src(revealJs, { "base" : "node_modules/reveal.js" })
    .pipe(uglify())
    .pipe(gulp.dest(destinationScript));
});

gulp.task('gamepad', function (cb) {
  return gulp.src("src/script/gamepad.js")
    .pipe(uglify())
    .pipe(gulp.dest(pluginsDestination + '/gamepad'));
});

gulp.task('copyStyle', function () {
  return gulp.src(styleReveal, { "base" : "node_modules/reveal.js/css" })
    .pipe(postcss([cssnano()]))
    .pipe(gulp.dest(destinationStyle));
});

gulp.task('copyNotesHtml', function () {
  return gulp.src(notes, { "base" : "node_modules/reveal.js" })
    .pipe(gulp.dest(destinationScript));
});

gulp.task('copyPlugins', function () {
  return gulp.src(pluginsReveal, { "base" : "node_modules" })
    .pipe(uglify())
    .pipe(gulp.dest(pluginsDestination));
});

// IMAGES
gulp.task('images', function (cb) {
  return gulp.src('src/images/**/*')
  .pipe(gulp.dest('public/images'));
});

gulp.task('watch:images', ['images'], function () {
  browserSync.reload();
});

// JS
gulp.task('mainMasterjs', function (cb) {
  return gulp.src(mainMasterjs)
    .pipe(concat('main-master.js'))
    .pipe(rename("main.js"))
    .pipe(uglify())
    .pipe(gulp.dest(destinationScript));
});

gulp.task('mainjs', function (cb) {
  return gulp.src(mainjs)
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest(destinationScript));
});

gulp.task('watch:js', ['mainjs'], function () {
  browserSync.reload();
});

// SASS
gulp.task('sass', function () {
  return gulp.src('src/style/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(cssPlugins))
    .pipe(gulp.dest('public/style'))
    .pipe(gulp.dest('public/master/style'));
});

gulp.task('watch:sass', ['sass'], function () {
  browserSync.reload();
});

// PUG
gulp.task('pug', ['lint:pug'],function () {
  return gulp.src('src/index.pug')
    .pipe(pug({
      client: false,
      doctype: 'html',
      pretty: true
    }))
    .pipe(htmlmin(minOptions))
    .pipe(gulp.dest(destination));
});

gulp.task('lint:pug', function () {
  return gulp.src('src/index.pug')
    .pipe(pugLinter())
    .pipe(pugLinter.reporter())
});

gulp.task('watch:pug', ['pug'], function () {
  browserSync.reload();
});

// CLEAN
gulp.task('clean:public', function () {
  return gulp.src('public')
    .pipe(clean())
});

gulp.task('clean:archive', function () {
  return gulp.src('archive')
    .pipe(clean())
});

gulp.task('clean', ['clean:public', 'clean:archive']);

gulp.task('surge', function() {
  return surge({
    project: './public',
    domain: surgeUrl
  })
});

gulp.task('archive', () =>
    gulp.src('public/*')
        .pipe(zip('presentation.zip'))
        .pipe(gulp.dest('archive'))
);

gulp.task('build', gulpSequence('clean', ['compress', 'images', 'mainjs', 'pug', 'sass']));
gulp.task('build:master', gulpSequence('clean', ['compress', 'images', 'mainMasterjs', 'pug', 'sass']));

gulp.task('deploy', gulpSequence('build', 'surge'));

gulp.task('zip', gulpSequence('build', 'archive'));

gulp.task('serve', ['build'], function() {

    browserSync.init({
      server: "./public"
    });

    gulp.watch("src/style/**/*.scss", ['watch:sass']);
    gulp.watch("src/script/**/*.js", ['watch:js']);
    gulp.watch("src/index.pug", ['watch:pug']);
});

gulp.task('serve:master', ['build:master'], function() {

  browserSync.init({
    server: "./public",
    // https: true
  });

  gulp.watch("src/style/**/*.scss", ['watch:sass']);
  gulp.watch("src/script/**/*.js", ['watch:js']);
  gulp.watch("src/index.pug", ['watch:pug']);
});