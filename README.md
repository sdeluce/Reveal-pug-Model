# Présentation RevealJs de base

## Pour commencer

Pour utiliser ce projet vous devez avoir NodeJs, Gulp et Yarn installé sur votre machine.
Ensuite tapez la commande suivante pour installer les dépendances.

```bash
yarn
```

## Commandes gulp

```bash
gulp serve
gulp build
gulp deploy
gulp zip
```
